import time
import xml.etree.ElementTree as Et
import os
import tkinter
import tkinter.ttk as ttk
import style as style

#Document config reading
tree = Et.parse('conf.xml')
root = tree.getroot()

# List of routers
networkTopology = []

global nbRouterOnTheConfigFile

# -------------------------------------------------- UI --------------------------------------------------

class Neighbor:
    def __init__(self, frame, neighbor_line, name, interface, routerParent, metric):
        self.frame = frame
        self.ligne = neighbor_line
        self.name = name
        self.interface = interface
        self.routerParent = routerParent
        self.metric = metric
        self.create_neighbor(neighbor_line)

    def create_neighbor(self, line):
        if self.name != -1:
            name = tkinter.StringVar()
            name.set(self.name)
            self.ligne +=1
        else:
            name = tkinter.StringVar()
            name.set("Neighbor " + str(self.ligne))
            self.name = str(name.get())

        b0 = tkinter.Entry(self.frame, textvariable=name, width=15)

        interfaceValues = ["fastEth", "Eth1", "Eth2", "Eth3"]
        b1 = ttk.Combobox(self.frame, values=interfaceValues, width=10)
        if self.interface != -1:
            b1.current(self.interface)
        else:
            b1.current(0)
            self.interface = 0

        b2 = ttk.Button(self.frame, text="X", command=self.delete_neighbor, width=3)
        b3 = ttk.Button(self.frame, text="V", width=3, command=self.modify_intent)
        ospfMetric = tkinter.StringVar()
        ospfMetric.set(self.metric)
        b4 = tkinter.Entry(self.frame, textvariable=ospfMetric, width=10)
        b0.grid(row=self.ligne, column=0)
        b1.grid(row=self.ligne, column=1)
        b2.grid(row=self.ligne, column=3)
        b3.grid(row=self.ligne, column=4)
        b4.grid(row=self.ligne, column=2)
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

    def delete_neighbor(self):
        for router in root.findall('routers'):
            if router.get('name') == self.routerParent:
                for neighbor in router.findall('neighbor'):
                    if neighbor.get('name') == self.name:
                        router.remove(neighbor)
                        tree.write('conf.xml')
                        print("Neighbor", neighbor.get('name'), "deleted from the config file")
        self.frame.destroy()
        self.ligne -= 1
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

        # Adding a neighbor to the config file for this router
    def modify_intent(self):
        for router in root.findall('routers'):
            if router.get('name') == self.routerParent:
                for neighbor in router.findall('neighbor'):
                    # Update the neighbor if it already exists
                    if neighbor.get('name') == self.name:
                        self.name = self.frame.winfo_children()[0].get()
                        self.interface = self.frame.winfo_children()[1].current()
                        self.metric = self.frame.winfo_children()[4].get()
                        neighbor.set('name', self.name)
                        neighbor.set('interface', str(self.interface))
                        neighbor.set('metric', self.metric)
                        tree.write('conf.xml')
                        print("Neighbor", neighbor.get('name'), "updated in the config file")
                        frame_buttons.update_idletasks()
                        canvas.config(scrollregion=canvas.bbox("all"))
                        return
                self.name = self.frame.winfo_children()[0].get()
                self.interface = self.frame.winfo_children()[1].current()
                self.metric = self.frame.winfo_children()[4].get()
                newNeighbor = Et.SubElement(router, 'neighbor', name=str(self.name), interface=str(self.interface))
                print("Neighbor", newNeighbor.get('name'), "added to the config file")
                tree.write('conf.xml')
                frame_buttons.update_idletasks()
                canvas.config(scrollregion=canvas.bbox("all"))

class Intent:
    def __init__(self, frame, router_ligne, nameOfThisRouter, asOfThisRouter):
        self.frame = frame
        self.router_ligne = router_ligne
        self.j = 1
        self.nameOfThisRouter = nameOfThisRouter
        self.asOfThisRouter = asOfThisRouter
        self.creation()


    def creation(self):
        nameOfThisRouter = tkinter.StringVar()
        nameOfThisRouter.set(self.nameOfThisRouter)
        asOfThisRouter = tkinter.StringVar()
        asOfThisRouter.set(self.asOfThisRouter)
        e0 = tkinter.Entry(self.frame, textvariable=nameOfThisRouter, width=15)
        e1 = tkinter.Entry(self.frame, textvariable=asOfThisRouter, width=10)
        b0 = ttk.Button(self.frame, text="Add a neighbor", command=self.addNeighbor)
        b1 = ttk.Button(self.frame, text="X", width=3, command=self.delete_frame)
        b2 = ttk.Button(self.frame, text="V", width=3, command=self.modify_intent)
        e0.grid(row=0, column=0)
        e1.grid(row=0, column=1)
        b0.grid(row=0, column=2)
        b1.grid(row=0, column=3)
        b2.grid(row=0, column=4)


    def addNeighbor(self):
        i = tkinter.Frame()
        i.grid(columnspan=5, in_ = self.frame)
        Neighbor(i, self.j, -1, -1, self.nameOfThisRouter, 0)
        self.j += 1

    def delete_frame(self):
        global nbRouterOnTheConfigFile
        for router in root.findall('routers'):
            if router.get('name') == self.nameOfThisRouter:
                root.remove(router)
                tree.write('conf.xml')
                print("Router", router.get('name'), "deleted from the config file")
        self.frame.destroy()
        nbRouterOnTheConfigFile -= 1
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

    # Adding the router to the config file if not already in
    def modify_intent(self):
        for router in root.findall('routers'):
            # Updating the name and AS of the router if already in the config file
            if router.get('name') == self.nameOfThisRouter:
                self.nameOfThisRouter = self.frame.winfo_children()[0].get()
                self.asOfThisRouter = self.frame.winfo_children()[1].get()
                router.set('name', self.nameOfThisRouter)
                router.set('AS', self.asOfThisRouter)
                print("Router", router.get('name'), "updated in the config file")
                tree.write('conf.xml')
                frame_buttons.update_idletasks()
                canvas.config(scrollregion=canvas.bbox("all"))
                return
        self.nameOfThisRouter = self.frame.winfo_children()[0].get()
        self.asOfThisRouter = self.frame.winfo_children()[1].get()
        newRouter = Et.SubElement(root, 'routers', name=self.nameOfThisRouter, AS=self.asOfThisRouter)
        tree.write('conf.xml')
        print("Router", newRouter.get('name'), "added to the config file")
        frame_buttons.update_idletasks()
        canvas.config(scrollregion=canvas.bbox("all"))

def new_router():
    global nbRouterOnTheConfigFile
    global configurationRouter
    h = tkinter.Frame(frame_buttons)
    h.grid()
    nbRouterOnTheConfigFile += 1
    nameOfThisRouter = "Router " + str(nbRouterOnTheConfigFile)
    asOfThisRouter = "AS here"
    configurationRouter.append(Intent(h, nbRouterOnTheConfigFile ,nameOfThisRouter, asOfThisRouter))
    frame_buttons.update_idletasks()
    canvas.config(scrollregion=canvas.bbox("all"))


# -------------------------------------------------- Declaration of class --------------------------------------------------

#Class to define each router and these attributes
class Routers:
    def __init__(self, name, neighbors, protocol, name_of_as, border, border_neighbors, nb_ports):
        self.name = name
        self.neighborsList = neighbors
        self.protocol = protocol
        self.AS = name_of_as
        self.border = border
        self.bn = border_neighbors
        self.ports = [0] * nb_ports
        self.addressing = [0] * nb_ports
        self.metric = [0] * nb_ports

    def __repr__(self):
        return f"{self.name} {self.neighborsList} {self.protocol} {self.AS} " \
               f"{self.border} {self.bn} {self.ports} {self.addressing} {self.metric} "

# To get the index of the end of the network prefix
def get_network_prefix(address):
    mask = address[-2:]
    nbColon, position = 0, 0
    while nbColon != (int(mask) / 16):
        position += 1
        if address[position] == ":":
            nbColon += 1
    return position

def get_router_on_the_same_as(router_name):
    router_iter = 0
    while networkTopology[router_iter].name != router_name:
        router_iter += 1
    thisAS = networkTopology[router_iter].AS
    inTheSameAS = []
    for router in networkTopology:
        if router.AS == thisAS and router.name != router_name:
            inTheSameAS.append(router)
    return inTheSameAS


def main():
    # -------------------------------------------------- Initialisation --------------------------------------------------

    # Define an object for each router with his name and neighbors
    i = 0
    for routers in root.findall('routers'):
        networkTopology.append(Routers('',[],'','',False,[],4))
        networkTopology[i].name = routers.get('name')
        networkTopology[i].AS = routers.get('AS')

        # List the neighbors of this router and the interface
        for neighbor in routers.iter('neighbor'):
            networkTopology[i].neighborsList.append(neighbor.get('name'))
            networkTopology[i].ports[int(neighbor.get('interface'))] = neighbor.get('name')
            if neighbor.get('metric') is not None:
                networkTopology[i].metric[int(neighbor.get('interface'))] = neighbor.get('metric')
        i += 1

    # Define for each router, the AS and protocol
    for autonomousSystem in root.findall('AutonomousSys'):
        nameOfThisAS = autonomousSystem.get('name')
        protocolInThisAS = autonomousSystem.find('protocol').text

        # Find the network prefix
        addressingInThisAS = autonomousSystem.find('addressing').text
        k = get_network_prefix(addressingInThisAS)

        subNetwork = 1

        # Set the protocol of each route
        for i in range(len(networkTopology)):
            if nameOfThisAS == networkTopology[i].AS:
                networkTopology[i].protocol = protocolInThisAS

        # Put the address of each interface
        for routerIter in range(len(networkTopology)):
            for neighborIter in range(4):
                if (networkTopology[routerIter].ports[neighborIter] != 0) and (networkTopology[routerIter].addressing[neighborIter] == 0) :
                    m = 0
                    while networkTopology[routerIter].ports[neighborIter] != networkTopology[m].name:
                        m += 1

                    # Only put the address on router on the same AS
                    if networkTopology[m].AS == networkTopology[routerIter].AS:
                        networkTopology[routerIter].addressing[neighborIter] = str(addressingInThisAS[:k + 1]) + str(subNetwork) + "::" + str(routerIter + 1)
                        n = networkTopology[m].ports.index(networkTopology[routerIter].name)
                        networkTopology[m].addressing[n] = str(addressingInThisAS[:k+1]) + str(subNetwork) + "::" + str(m+1)
                        subNetwork += 1

    # Define if a router is a border router and these borders neighbor
    for i in range(len(networkTopology)):
        for j in range(len(networkTopology[i].neighborsList)):
            k = 0
            while networkTopology[i].neighborsList[j] != networkTopology[k].name:
                k += 1
            if networkTopology[k].AS != networkTopology[i].AS:
                networkTopology[i].border = True
                networkTopology[i].bn.append(networkTopology[i].neighborsList[j])

    # Define network between AS
    for routerBorder in root.find('interAsAddressing'):
        for routerAddressing in routerBorder.iter('addressing'):
            use = False # To put one address by pair of border router
            addressingBetweenAS = routerAddressing.text
            k = get_network_prefix(addressingBetweenAS)

            for routerIter in range(len(networkTopology)):
                if networkTopology[routerIter].border:
                    m = 0
                    while networkTopology[routerIter].bn[0] != networkTopology[m].name:
                        m += 1
                    portNumberOne = networkTopology[routerIter].ports.index(networkTopology[routerIter].bn[0])

                    if networkTopology[routerIter].addressing[portNumberOne] == 0 and not use:
                        use = True
                        networkTopology[routerIter].addressing[portNumberOne] = str(addressingBetweenAS[:k+1]) + ":" + str(routerIter+1)
                        portNumberTwo = networkTopology[m].ports.index(networkTopology[routerIter].name)
                        networkTopology[m].addressing[portNumberTwo] = str(addressingBetweenAS[:k + 1]) + ":" + str(m + 1)


    for i in range(len(networkTopology)):
        print(networkTopology[i])
    #   print(get_router_on_the_same_as("R9"))

    # -------------------------------------------------- Modifying files --------------------------------------------------

    # Créatinon du dictionnaire avec folder + num de routeur associé
    folder_path = "project-files/dynamips"

    for i in range(len(networkTopology)):
        # Setting up file names
        nomFichier = "i"+str(i+1)+"_startup-config.cfg"
        print("Updating "+nomFichier)

        # Opening files
        fichier = open(nomFichier, "w")
        default1 = open("Default1.txt","r")
        default2 = open("Default2.txt","r")
        default3 = open("Default3.txt","r")
        default4 = open("Default4.txt","r")

        # Add lines before host
        for line in default1:
            fichier.write(line)

        # Add hostname
        fichier.write("\nhostname "+networkTopology[i].name+"\n")

        # Add everything else before interfaces
        for line in default2:
            fichier.write(line)

        # Add Interfaces
        toWriteInterfaces = ""
        for j in range(4):
            if j == 0: # If Interface is Fast
                toWriteInterfaces += "\ninterface FastEthernet0/0\n"
            else: # If Interface is Giga
                toWriteInterfaces += "interface GigabitEthernet"+str(j)+"/0\n"

            # If interface unused
            if networkTopology[i].addressing[j] == 0:
                toWriteInterfaces += " no ip address\n shutdown\n"
                if networkTopology[i].protocol == "RIP":
                    toWriteInterfaces += " duplex full\n!\n"
                else:
                    toWriteInterfaces += " negotiation auto\n!\n"

            # If interface used
            else:
                toWriteInterfaces +=  " no ip address\n"
                if networkTopology[i].protocol == "RIP":
                    toWriteInterfaces += " duplex full\n ipv6 address "+str(networkTopology[i].addressing[j])+"/64\n ipv6 enable\n ipv6 rip AS"+str(networkTopology[i].AS)+" enable\n!\n"
                else:
                    toWriteInterfaces += " negotiation auto\n ipv6 address "+str(networkTopology[i].addressing[j])+"/64\n ipv6 enable\n"
                    if "2001:100:1:" in networkTopology[i].addressing[j]: # On met OSPF seulement si l'interface reste dans le même AS
                        toWriteInterfaces += " ipv6 ospf "+str(i+1)+" area 0\n"+" ipv6 ospf cost " + networkTopology[i].metric[j] +"\n"
                    if "2001:100:2:" in networkTopology[i].addressing[j]: # On met OSPF seulement si l'interface reste dans le même AS
                        toWriteInterfaces += " ipv6 ospf "+str(i+1)+" area 0\n"+" ipv6 ospf cost " + networkTopology[i].metric[j] + "\n"
                    toWriteInterfaces += "!\n"

        fichier.write(toWriteInterfaces)

        # BGP
        fichier.write("router bgp "+str(networkTopology[i].AS)+"\n")
        fichier.write(" bgp router-id "+str(i+1)+"."+str(i+1)+"."+str(i+1)+"."+str(i+1)+"\n")
        fichier.write(" bgp log-neighbor-changes\n")
        fichier.write(" no bgp default ipv4-unicast\n")

        ### Add declare neighbors
        sameASRouters = get_router_on_the_same_as(networkTopology[i].name)
        if networkTopology[i].border == True:
            for j in range(len(sameASRouters)):
                if sameASRouters[j].border == False: # Ne pas annoncer les routeurs de bordure du même AS comme étant voisins
                    # In our case, at least interface 0 or 1 is open (and seems conventionnally legit)
                    if sameASRouters[j].addressing[0] != 0:
                        fichier.write(" neighbor "+str(sameASRouters[j].addressing[0]))
                    else:
                        fichier.write(" neighbor "+str(sameASRouters[j].addressing[1]))
                    fichier.write(" remote-as "+str(sameASRouters[j].AS)+"\n")


        else:
            for j in range(len(sameASRouters)):
                # In our case, at least interface 0 or 1 is open (and seems conventionnally legit)
                if sameASRouters[j].addressing[0] != 0:
                    fichier.write(" neighbor "+str(sameASRouters[j].addressing[0]))
                else:
                    fichier.write(" neighbor "+str(sameASRouters[j].addressing[1]))
                fichier.write(" remote-as "+str(sameASRouters[j].AS)+"\n")

        # Add neighbors in other AS
        listeVoisins = networkTopology[i].neighborsList
        for j in range (len(listeVoisins)): # Parcours de la liste des voisins de i
            for k in range(len(networkTopology)):
                if listeVoisins[j] == networkTopology[k].name: # Les voisins de i ont l'indice k
                    if (networkTopology[i].AS != networkTopology[k].AS): # If neighbor is in a different AS
                        neighborAddress = ""
                        for l in range (len(networkTopology[k].ports)): # To find the interface between the 2 neighbors
                            if (networkTopology[k].ports[l] == networkTopology[i].name):
                                neighborAddress = str(networkTopology[k].addressing[l])
                                #print(networkTopology[k].ports)
                                #print(i, j, k, l)
                        fichier.write(" neighbor "+neighborAddress)
                        fichier.write(" remote-as " +str(networkTopology[k].AS)+"\n")

        fichier.write(" !\n address-family ipv4\n exit-address-family\n !\n")
        fichier.write(" address-family ipv6\n")
        ###

        # Add declaring networks
        listeReseaux = []
        if networkTopology[i].border == True:
            for j in range (len(networkTopology)):
                if networkTopology[j].AS == networkTopology[i].AS: # Pour chaque routeur dans le même AS que notre routeur de bordure
                    for k in range (len(networkTopology[j].addressing)): # On parcourt la liste des interfaces de chaque routeur
                        if (networkTopology[j].addressing[k]) != 0:
                            addWithMask = str(networkTopology[j].addressing[k])+"/64"
                            slicePos = get_network_prefix(addWithMask)
                            slice_text = slice(slicePos+2) # On trouve la position où couper l'addresse avec masque
                            if addWithMask[slice_text]+"/64" not in listeReseaux: # Si le réseau n'est pas déjà dans la liste de réseaux à annoncer
                                listeReseaux.append(addWithMask[slice_text]+"/64")

        for j in range (len(listeReseaux)):
            fichier.write("  network "+listeReseaux[j]+"\n")
        #


        ### Add activate neighbors
        if networkTopology[i].border == True:
            for j in range(len(sameASRouters)):
                if sameASRouters[j].border == False: # Ne pas annoncer les routeurs de bordure du même AS comme étant voisins
                    # In our case, at least interface 0 or 1 is open (and seems conventionnally legit)
                    if sameASRouters[j].addressing[0] != 0:
                        fichier.write("  neighbor "+str(sameASRouters[j].addressing[0]))
                    else:
                        fichier.write("  neighbor "+str(sameASRouters[j].addressing[1]))
                    fichier.write(" activate\n")


        else:
            for j in range(len(sameASRouters)):
                # In our case, at least interface 0 or 1 is open (and seems conventionnally legit)
                if sameASRouters[j].addressing[0] != 0:
                    fichier.write("  neighbor "+str(sameASRouters[j].addressing[0]))
                else:
                    fichier.write("  neighbor "+str(sameASRouters[j].addressing[1]))
                fichier.write(" activate\n")

        # Add neighbors in other AS
        listeVoisins = networkTopology[i].neighborsList
        for j in range (len(listeVoisins)): # Parcours de la liste des voisins de i
            for k in range(len(networkTopology)):
                if listeVoisins[j] == networkTopology[k].name: # Les voisins de i ont l'indice k
                    if (networkTopology[i].AS != networkTopology[k].AS): # If neighbor is in a different AS
                        neighborAddress = ""
                        for l in range (len(networkTopology[k].ports)): # To find the interface between the 2 neighbors
                            if (networkTopology[k].ports[l] == networkTopology[i].name):
                                neighborAddress = str(networkTopology[k].addressing[l])
                                #print(networkTopology[k].ports)
                                #print(i, j, k, l)
                        fichier.write("  neighbor "+neighborAddress)
                        fichier.write(" activate\n")

        fichier.write(" exit-address-family\n!\n")

        # Add thing before redistribute connected
        for line in default3:
            fichier.write(line)

        # Add redistribute connected or router-id
        if networkTopology[i].protocol == "RIP":
            fichier.write("\nipv6 router rip AS"+str(networkTopology[i].AS)+"\n redistribute connected\n")
        else:
            fichier.write("\nipv6 router ospf "+str(i+1)+"\n router-id "+str(i+1)+"."+str(i+1)+"."+str(i+1)+"."+str(i+1)+"\n")

        # Add everything before line con 0
        for line in default4:
            fichier.write(line)


        # Closing files
        fichier.close()
        default1.close()
        default2.close()
        default3.close()
        default4.close()

    # Put files in good folder
    for i in range(len(networkTopology)):
        for path, dirs, files in os.walk(folder_path):
            for filename in files: # Pour tous les fichiers dans dynamips
                nameToHave = "i"+str(i+1)+"_startup"
                if nameToHave in filename: # Pour tous les fichiers startup-config
                    os.rename(filename,path+"/"+filename)
                    print("Moved "+filename+" to "+path+"/"+filename)


# Graphical interface
rootTk = tkinter.Tk()
nbRouterOnTheConfigFile = 0
rootTk.title("Network topology generator")
rootTk.geometry("600x600")
rootTk.resizable(width=False, height=False)

canvas = tkinter.Canvas(rootTk)
canvas.grid(row=0, column=0, sticky="news")

vsb = ttk.Scrollbar(rootTk, orient="vertical", command=canvas.yview)
vsb.grid(row=0, column=1, sticky='ns')
canvas.configure(yscrollcommand=vsb.set)

frame_buttons = ttk.Frame(canvas)
canvas.create_window((0, 0), window=frame_buttons, anchor='nw')

global configurationRouter
configurationRouter = []
configurationNeighborhood = [[]]

for router in root.findall('routers'):
    f = tkinter.Frame(frame_buttons)
    f.grid(row=nbRouterOnTheConfigFile, column=0)
    configurationRouter.append(Intent(f, nbRouterOnTheConfigFile + 1, router.get('name'), router.get('AS')))
    nbRouterOnTheConfigFile += 1
    nbNeighborForThisRouter = 0
    for neighbor in router.findall('neighbor'):
        i = tkinter.Frame()
        i.grid(columnspan=5, in_=f)
        configurationNeighborhood[nbRouterOnTheConfigFile - 1].append(Neighbor(i, nbNeighborForThisRouter, neighbor.get('name'), neighbor.get('interface'), router.get('name'), neighbor.get('metric')))
        nbNeighborForThisRouter += 1
    configurationNeighborhood.append([])

line = nbRouterOnTheConfigFile - 1

bLa = ttk.Button(rootTk, text="Launch", command=main) #Button to launch the program
bLa.grid(row=0, column=2)
bAdd = ttk.Button(rootTk, text="Add a router", command=new_router)
bAdd.grid(row=0, column=3)


frame_buttons.update_idletasks()
canvas.config(scrollregion=canvas.bbox("all"), width=400, height=550)

rootTk.mainloop()